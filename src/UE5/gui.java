package UE5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class gui implements ActionListener {
    private JFrame frame;
    private JButton btn;
    private JTextField txt;

    private void createAndShow() {
        /* links: ein Button mit Beschriftung */
        btn = new JButton();
        btn.setText("Run");
        /* rechts: ein Textfeld */
        txt = new JTextField();

        /* Hauptfenster */
        frame = new JFrame();
        /* Aktion beim schließen: JVM beenden */
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        /* Layout festlegen und Button & Textfeld hinzufügen */
        frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(),
                BoxLayout.X_AXIS));
        frame.getContentPane().add(btn);
        frame.getContentPane().add(txt);

        /* Größe setzen und anzeigen */
        frame.setPreferredSize(new Dimension(300, 100));
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        /* Im EDT ausführen: new SE3SimpleGui().createAndShow(); */
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new gui().createAndShow();
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.btn && this.btn.getText() == "start"){
            btn.setText("Stop");

        }
    }
}