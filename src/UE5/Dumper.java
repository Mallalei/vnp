package UE5;

public class Dumper extends Thread{

    private MultiThreaded threadInstance;
    private long delay = 300;
    // VOlatile Variable, um den Thread zu beenden
    volatile boolean started;
    public Dumper(MultiThreaded threadInstance){
        this.threadInstance = threadInstance;
        started = true;
        this.start();
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    public void run(){
        while(started){
            System.out.print("[" +threadInstance.status+ "]");
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

}
