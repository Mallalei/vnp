package UE5;

public class MultiThreaded{
	volatile int status = 0;
	static Dumper dumperThread;

	public static void main(String[] args) {
		System.out.print("BEGIN");
		MultiThreaded multiThreaded = new MultiThreaded();
		dumperThread = new Dumper(multiThreaded);
		multiThreaded.executeComputation();
		System.out.print("END");
	}

	public void evaluateInput() {
		int input;
		while (true) {
			try {
				input = System.in.read();
			} catch (Exception e) { input = 0; }

			switch(input) {
				case '1':
				case '2':
				case '3':
					status = input - '0';
					break;
				case 'q':
					dumperThread.started = false;
					return;
				default:
			}
		}
	}

	public void executeComputation(){
		while(true){
			status++;
		}
	}

} // class UE5.MultiThreaded


//JVM terminiert nicht, da der UE5.Dumper Thread zwar gestartet wird aber nicht beim Beenden der evaluateInput-Methode gestoppt wird.

//d) Realisierung in nur einer Klassendefinition, indem UE5.MultiThreaded Thread extended und dann die Run Methode von UE5.Dumper übernommen wird und diese in der evaluateInput Methode aufgerufen wird.

//a3 Mit run() wird nur die Methode aus der Klasse verwendet und ausgeführet, mit start() wird ein echter neuer Thread erzeugt, in dem die Metzhode run() gestartet wird.

//a4 Mit der Stop Methode