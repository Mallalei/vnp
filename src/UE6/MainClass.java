package UE6;

public class MainClass {

    static Kunde kunde1;
    static Kunde kunde2;

    static Konto konto = new Konto(0);

    public static Kontoverwaltung kv1 = new Kontoverwaltung(konto);
    public static Kontoverwaltung kv2 = new Kontoverwaltung(konto);

    public static void main(String[] args) {
        kunde1 = new Kunde("Sascha", kv1);
        kunde2 = new Kunde("Luis", kv2);
        System.out.println("Initial-Kontostand 1: " + kv1.getKontostand());
        System.out.println("Initial-Kontostand 2: " + kv2.getKontostand());
        kunde1.start();
        kunde2.start();


    }

}
