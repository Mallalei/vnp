package UE6;

public class Kontoverwaltung {
    volatile boolean lock = false;
    volatile int ilock = 1;
    private Konto konto;
    public Kontoverwaltung(Konto konto) {
        this.konto = konto;
    }
    public int getKontostand(){
        return konto.getKontostand();
    }
// Booleon Lock
    public void einzahlen(int summe) {
        int kontostand;
        if (!lock) {
            lock = true;
            kontostand = konto.getKontostand();
            kontostand += summe;
            konto.setKontostand(kontostand);
            lock = false;
        }
    }
    public void abheben(int summe) {
        int kontostand;
        if (!lock) {
            lock = true;
            kontostand = konto.getKontostand();
            kontostand -= summe;
            konto.setKontostand(kontostand);
            lock = false;
        }
    }
// Integer Lock
//    public void einzahlen(int summe) {
//        int kontostand;
//        if (--ilock >= 0) {
//            kontostand = konto.getKontostand();
//            kontostand += summe;
//            konto.setKontostand(kontostand);
//            System.out.println(getKontostand());
//            } ilock++;
//        }
//    public void abheben(int summe) {
//        int kontostand;
//        if (--ilock >= 0) {
//            kontostand = konto.getKontostand();
//            kontostand -= summe;
//            konto.setKontostand(kontostand);
//            System.out.println(getKontostand());
//            } ilock++;
//        }
}