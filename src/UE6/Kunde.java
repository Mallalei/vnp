package UE6;

public class Kunde extends Thread {
    private Kontoverwaltung kv;
    private String name;
    public Kunde(String name, Kontoverwaltung kontoverwaltung) {
        this.kv = kontoverwaltung;
        this.name = name;
    }

    public void run(){
        while(true){
            if (kv.getKontostand() < 30){
            kv.einzahlen(1);
//                System.out.println("Kontostand nach Aktion von "+ name + ": " + kv.getKontostand());
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

            }
            else {
                kv.abheben(25);
//                System.out.println("Kontostand nach Aktion von "+ name + ": "+ kv.getKontostand());
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

}
