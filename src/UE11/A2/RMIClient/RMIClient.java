package UE11.A2.RMIClient;
// Remote Service: verteilte L�sung mit RMI-Nutzung

import UE11.A2.RMIServer.Account;
import UE11.A2.RMIServer.Datum;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RMIClient {

    public static void main(String[] args) throws Exception {

        Registry registry = LocateRegistry.getRegistry();
        // Registry registry = LocateRegistry.getRegistry("vsnb105", 1099);
        // hier: lokales Netz, Host-Id ohne Domain-Id (keine URL! vgl.
        // Naming.java)
        // extern: vsnb105.cs.uni-kl.de (keine URL! vgl. Naming.java)
        // Achtung: Firewall muss u.U. nachkonfiguriert werden;
        // Windows: Ausnahme Java Platform SE binary (f�r
        // C:\Programme\Java\jdk1.6.0_16\bin\rmiregistry.exe)
        System.out.println("RMIClient: RMIregistry lokalisiert");

        String[] liste = registry.list();
        for (String i : liste) {
            System.out.println(i);
        }

        Datum datum = (Datum) registry.lookup("Datum");
        Account account = (Account) registry.lookup("Account");

        System.out.println("Datum " + datum.getTime());
        System.out.println("Account - deposit: " + account.deposit(20));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        System.out.println("Datum " + datum.getTime());
        System.out.println("Account - withdraw: " + account.withdraw(30));
    }
}
