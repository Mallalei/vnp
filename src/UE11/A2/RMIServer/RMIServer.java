package UE11.A2.RMIServer;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class RMIServer {

    // Konstruktor für Serverobjekte
    public RMIServer() {
        super();
    }
    // main: Dienste exportieren und registrieren
    public static void main(String[] args) throws Exception {
        try {
            DatumImpl obj1 = new DatumImpl();
            AccountImpl obj2 = new AccountImpl();
            // Remote Objects exportieren, um sie für eingehende Aufrufe
            // bereitzustellen; Kreierung von Servicethreads mit den
            // Objektreferenzen "obj1" und "obj2" und anonymen
            // Portnummern (2. Parameter = 0) (falls der Server hinter
            // einer Firewall platziert ist, kann es sinnvoll sein, diese
            // Portnummern fest einzustellen)
            Datum stub1 = (Datum) UnicastRemoteObject.exportObject(obj1, 0);
            System.out.println("RMIServer - main: Datum exportiert");
            Account stub2 = (Account) UnicastRemoteObject.exportObject(obj2, 0);
            System.out.println("RMIServer - main: Account exportiert");
            // Erzeugung des rmiRegistry-Dienstes auf dem lokalen Host
            // Default Port: 1099
            Registry registry = LocateRegistry.createRegistry(1099);
            System.out.println("RMIServer - main: Registry kreiert");
            // Registrierung der Dienste Datum und Account
            registry.bind("Datum", stub1);
            System.out.println("RMIServer - main: Datum registriert");
            registry.bind("Account", stub2);
            System.out.println("RMIServer - main: Account registriert");
        } catch (Exception e) {
            System.err.println("RMIServer exception:");
            e.printStackTrace();
        }
        // main terminiert; die JVM existiert weiter, mit den
        // Threads zur Verwaltung der exportierten Objekte
        // sowie registry
        System.out.println("RMIServer - main: Terminierung");
    }
}
