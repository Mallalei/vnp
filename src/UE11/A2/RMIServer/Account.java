package UE11.A2.RMIServer;
// Interface Account

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Account extends Remote {

    int deposit(int x) throws RemoteException;

    int withdraw(int x) throws RemoteException;
}
