package UE11.A2.RMIServer;
// Interface Datum

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;

public interface Datum extends Remote {
    Date getTime() throws RemoteException;
}
