package UE11.A2.RMIServer;

import java.rmi.RemoteException;
import java.util.Date;

public class DatumImpl implements Datum{
    @Override
    public Date getTime() throws RemoteException {
        System.out.println("Datum: getTime aufgerufen");
        return new Date();
    }
}
