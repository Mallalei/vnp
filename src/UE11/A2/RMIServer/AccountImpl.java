package UE11.A2.RMIServer;

import java.rmi.RemoteException;

public class AccountImpl implements Account{
    private int balance = 0;
    @Override
    public synchronized int  deposit(int x) throws RemoteException {
        System.out.println("Account: deposit aufgerufen");
        balance += x;
        return balance;
    }

    @Override
    public synchronized int withdraw(int x) throws RemoteException {
        System.out.println("Account: withdraw aufgerufen");
        balance -= x;
        return balance;
    }
}
