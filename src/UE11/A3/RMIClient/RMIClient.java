package UE11.A3.RMIClient;
// Remote Service: verteilte L�sung mit RMI-Nutzung

import UE11.A3.RMIServer.KlausurErgebnis;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RMIClient {
    private int matrikelNr;

    public static void main(String[] args) throws Exception {
        Registry registry = LocateRegistry.getRegistry(7553);
        // Registry registry = LocateRegistry.getRegistry("vsnb105", 1099);
        // hier: lokales Netz, Host-Id ohne Domain-Id (keine URL! vgl.
        // Naming.java)
        // extern: vsnb105.cs.uni-kl.de (keine URL! vgl. Naming.java)
        // Achtung: Firewall muss u.U. nachkonfiguriert werden;
        // Windows: Ausnahme Java Platform SE binary (f�r
        // C:\Programme\Java\jdk1.6.0_16\bin\rmiregistry.exe)
        System.out.println("RMIClient: RMIregistry lokalisiert");
        String[] liste = registry.list();
        for (String i : liste) {
            System.out.println(i);
        }
        KlausurErgebnis klausurErgebnis = (KlausurErgebnis) registry.lookup("KlausurErgebnis");

        if (klausurErgebnis.setErgebnis("open", 12345, 2.7)) {
            System.out.println("Ergebnis eingetragen.");
        } else {
            System.out.println("Fehler beim eintragen des Ergebnisses.");
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
        if (klausurErgebnis.setErgebnis("open", 54321, 3.7)) {
            System.out.println("Ergebnis eingetragen.");
        } else {
            System.out.println("Fehler beim eintragen des Ergebnisses.");
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
        if (klausurErgebnis.setErgebnis("open", 23451, 1.4)) {
            System.out.println("Ergebnis eingetragen.");
        } else {
            System.out.println("Fehler beim eintragen des Ergebnisses.");
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
        if (klausurErgebnis.setErgebnis("open", 32451, 1.0)) {
            System.out.println("Ergebnis eingetragen.");
        } else {
            System.out.println("Fehler beim eintragen des Ergebnisses.");
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }
        if (klausurErgebnis.getErgebnis(12345, "simsala") == -1) {
            System.out.println("Das Ergebnis fuer 12345 liegt noch nicht vor.");
        } else {
            System.out.println("Klausur ergebnis fuer 12345: " + klausurErgebnis.getErgebnis(12345, "simsala"));
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        if (klausurErgebnis.getErgebnis(32451, "simsala") == -1) {
            System.out.println("Das Ergebnis fuer 32451 liegt noch nicht vor.");
        } else {
            System.out.println("Klausur ergebnis fuer 32451: " + klausurErgebnis.getErgebnis(32451, "simsala"));
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        if (klausurErgebnis.getErgebnis(33333, "simsala") == -1) {
            System.out.println("Das Ergebnis fuer 33333 liegt noch nicht vor.");
        } else {
            System.out.println("Klausur ergebnis fuer 33333: " + klausurErgebnis.getErgebnis(33333, "simsala"));
        }
        if (klausurErgebnis.setErgebnis("open", 33333, 1.8)) {
            System.out.println("Ergebnis eingetragen.");
        } else {
            System.out.println("Fehler beim eintragen des Ergebnisses.");
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
        if (klausurErgebnis.getErgebnis(33333, "simsala") == -1) {
            System.out.println("Das Ergebnis fuer 33333 liegt noch nicht vor.");
        } else {
            System.out.println("Klausur ergebnis fuer 33333: " + klausurErgebnis.getErgebnis(33333, "simsala"));
        }
        if (klausurErgebnis.setErgebnis("open", 12345, 1.8)) {
            System.out.println("Ergebnis eingetragen.");
        } else {
            System.out.println("Fehler beim eintragen des Ergebnisses.");
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
        if (klausurErgebnis.getErgebnis(12345, "simsala") == -1) {
            System.out.println("Das Ergebnis fuer 12345 liegt noch nicht vor.");
        } else {
            System.out.println("Klausur ergebnis fuer 12345: " + klausurErgebnis.getErgebnis(33333, "simsala"));
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
    }
}