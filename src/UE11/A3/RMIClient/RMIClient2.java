package UE11.A3.RMIClient;
// Remote Service: verteilte L�sung mit RMI-Nutzung

import UE11.A3.RMIServer.KlausurErgebnis;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.InputMismatchException;
import java.util.Scanner;

public class RMIClient2 {
    private static Integer matrikelEingabe;
    private static String Option;
    private static Double ergebnisEingabe;
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        Registry registry = LocateRegistry.getRegistry(7553);
        System.out.println("RMIClient: RMIregistry lokalisiert");
        String[] liste = registry.list();
        for (String i : liste) {
            System.out.println(i);
        }
        KlausurErgebnis klausurErgebnis = (KlausurErgebnis) registry.lookup("KlausurErgebnis");
        while (true) {
            System.out.print("Was möchten Sie tun? ['eingabe' oder 'abfrage']: ");
            Option = sc.next();
            if (Option.equals("eingabe")) {
                System.out.println("Bitte geben Sie das Master Passwort ein: ");
                String masterPwdEingabe = sc.next();
                System.out.println("Bitte geben Sie die Matrikel nummer ein: ");
                getMatrikelNr();
                System.out.println("Bitte geben Sie das Ergebnis ein: ");
                getNote();
                if (klausurErgebnis.setErgebnis(masterPwdEingabe, matrikelEingabe, ergebnisEingabe)) {
                    System.out.println("Ergebnis eingetragen.");
                    System.out.println("*** Persönliches Passwort für " + matrikelEingabe + ": " + klausurErgebnis.getUserPwd(matrikelEingabe));
                } else {
                    System.out.println("Fehler beim eintragen des Ergebnisses.");
                }

            } else if (Option.equals("abfrage")) {
                System.out.println("Bitte geben Sie die Matrikel nummer ein: ");
                int matrikelEingabe = sc.nextInt();
                System.out.println("Bitte geben Sie Ihr User Passwort nummer ein: ");
                String userPwdEingabe = sc.next();
                if (klausurErgebnis.getErgebnis(matrikelEingabe, userPwdEingabe) == -1) {
                    System.out.println("Das Ergebnis fuer " + matrikelEingabe + " liegt noch nicht vor.");
                } else {
                    System.out.println("Klausur ergebnis fuer " + matrikelEingabe + ": " + klausurErgebnis.getErgebnis(matrikelEingabe, userPwdEingabe));
                }
            } else {
                System.out.println("Bitte geben Sie 'abfrage' oder 'eingabe' ein.");
            }
        }

    }

    private static void getMatrikelNr() throws Exception {
        try {
            matrikelEingabe = sc.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Die Eingabe muss ein Integer Wert sein. Beispiel: 12345 " + e);
            sc.next();
            getMatrikelNr();
        }
    }

    private static void getNote() throws Exception {
        try {
            ergebnisEingabe = sc.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Die Eingabe muss ein Double Wert sein. Beispiel: 1,2 " + e);
            sc.next();
            getNote();
        }
    }
}