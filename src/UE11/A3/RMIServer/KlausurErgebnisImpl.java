package UE11.A3.RMIServer;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Random;

public class KlausurErgebnisImpl implements KlausurErgebnis {
    private ArrayList<Integer> studies = new ArrayList<Integer>();
    private ArrayList<Double> note = new ArrayList<Double>();
    private ArrayList<String> givenUserPwd = new ArrayList<String>();
    private String initialMasterPwd = "open";
    private String initialUserPwd = "simsala";

    @Override
    public boolean setErgebnis(String masterPwd, int matrikelNr, double ergebnis) throws RemoteException {
        System.out.println("KlausurErgebnisImpl: setErgebnis aufgerufen");
        if (checkMasterPwd(masterPwd)) {
            insertErgebnis(matrikelNr, ergebnis);
            System.out.println(givenUserPwd.get(getUserIndex(matrikelNr)));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public double getErgebnis(int matrikelNr, String userPwd) throws RemoteException {
        System.out.println("KlausurErgebnisImpl: getErgebnis aufgerufen");
        if (checkPwd(matrikelNr, userPwd)) {
            return queryErgebnis(matrikelNr);
        } else {
            return -1;
        }
    }

    @Override
    public String getUserPwd(int matrikelNr) throws RemoteException {
        return givenUserPwd.get(getUserIndex(matrikelNr));
    }

    private boolean checkMasterPwd(String masterPwd) {
        System.out.println("KlausurErgebnisImpl: checkMasterPwd aufgerufen");
        if (masterPwd.equals(initialMasterPwd)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkPwd(int matrikelNr, String userPwd) {
        System.out.println("KlausurErgebnisImpl: checkPwd aufgerufen");
        if (userPwd.equals(givenUserPwd.get(getUserIndex(matrikelNr))) && !(getUserIndex(matrikelNr) < 0)) {
            return true;
        } else {
            return false;
        }
    }

    private double queryErgebnis(int matrikelNr) {
        System.out.println("KlausurErgebnisImpl: queryErgebnis aufgerufen");
        return note.get(getUserIndex(matrikelNr));
    }

    private void insertErgebnis(int matrikelNr, double ergebnis) {
        int index = getUserIndex(matrikelNr);
        if (index < 0) {
            studies.add(matrikelNr);
            note.add(ergebnis);
            givenUserPwd.add(getUserIndex(matrikelNr), randomUserPwd());
        } else {
            note.set(index, ergebnis);
        }
    }

    private int getUserIndex(int matrikelNr) {
        System.out.println("KlausurErgebnisImpl: getUserIndex aufgerufen");
        int result = -1;
        for (int i = 0; i < studies.size(); i++) {
            if (studies.get(i) == matrikelNr) {
                result = i;
            }
        }
        return result;
    }

    private String randomUserPwd() {
        System.out.println("KlausurErgebnisImpl: randomUserPwd aufgerufen");
        Random rand = new Random();
        char[] cArr;
        int wordLength;
        wordLength = rand.nextInt(10) + 2;
        cArr = new char[wordLength];
        for (int j = 0; j < cArr.length; j++) {
            int i = rand.nextInt(3);
            switch (i) {
                case 0: // capital letters
                    cArr[j] = (char) (rand.nextInt(26) + 97);
                    break;
                case 1: // lower-case character
                    cArr[j] = (char) (rand.nextInt(26) + 65);
                    break;
                case 2: // digits
                    cArr[j] = (char) (rand.nextInt(10) + 48);
                    break;
            }
        }
        return new String(cArr);
    }
}
