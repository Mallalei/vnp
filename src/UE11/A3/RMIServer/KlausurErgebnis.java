package UE11.A3.RMIServer;

import java.rmi.*;

public interface KlausurErgebnis extends Remote {
    public boolean setErgebnis(String masterPwd, int matrikelNr, double ergebnis) throws RemoteException;

    public double getErgebnis(int matrikelNr, String userPwd) throws RemoteException;

    public String getUserPwd(int matrikelNr) throws RemoteException;
}
