package UE11.A3.RMIServer;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class RMIServer {

    // Konstruktor für Serverobjekte
    public RMIServer() {
        super();
    }

    // main: Dienste exportieren und registrieren
    public static void main(String[] args) throws Exception {
        try {
            KlausurErgebnisImpl obj1 = new KlausurErgebnisImpl();
            // Remote Objects exportieren, um sie für eingehende Aufrufe
            // bereitzustellen; Kreierung von Servicethreads mit den
            // Objektreferenzen "obj1" und "obj2" und anonymen
            // Portnummern (2. Parameter = 0) (falls der Server hinter
            // einer Firewall platziert ist, kann es sinnvoll sein, diese
            // Portnummern fest einzustellen)
            KlausurErgebnis stub1 = (KlausurErgebnis) UnicastRemoteObject.exportObject(obj1, 0);
            System.out.println("RMIServer - main: KlausurErgebnis exportiert");
            // Erzeugung des rmiRegistry-Dienstes auf dem lokalen Host
            // Default Port: 1099
            Registry registry = LocateRegistry.createRegistry(7553);
            System.out.println("RMIServer - main: Registry kreiert");
            // Registrierung der Dienste Datum und Account
            registry.bind("KlausurErgebnis", stub1);
            System.out.println("RMIServer - main: KlausurErgebnisse registriert");

        } catch (Exception e) {
            System.err.println("RMIServer exception:");
            e.printStackTrace();
        }
        // main terminiert; die JVM existiert weiter, mit den
        // Threads zur Verwaltung der exportierten Objekte
        // sowie registry
        System.out.println("RMIServer - main: Terminierung");
    }

}
