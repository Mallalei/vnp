package UE8;

public class CrossRoads {
public static void main(String [] args) {
        Street street0 = new Street(0);
        Street street1 = new Street(1);
        Street street2 = new Street(2);
        Street street3 = new Street(3);
        new Car(0, street1, street2).start();
        new Car(1, street1, street2).start();
        new Car(2, street2, street3).start();
        new Car(3, street3, street0).start();
        }
}
class Car extends Thread {
private Street ownStreet, rightStreet;
private int number;
Car(int n, Street own, Street right) {
                number = n;
                ownStreet = own;
                rightStreet = right;
                }
@Override
public void run() {
                while (true) {
                        /* ein Auto kann jederzeit ankommen, aber erst wieder abfahren,
* wenn die rechte Straße frei ist */
                        ownStreet.getStreet(number);
                        arrive(ownStreet);
                        rightStreet.getStreet(number);
                        depart(ownStreet);
                        rightStreet.releaseStreet(number);
                        ownStreet.releaseStreet(number);
                        }
                }
private void arrive(Street s) {
                System.out.println("Car " + number + " arrives at street " + s.getNumber());
                }
private void depart(Street s) {
                System.out.println("Car " + number + " departs from street " + s.getNumber());
                }
}


class Street {
private int number, carNumber;
private boolean free = true;
public Street(int n) {
                number = n;
                }
public int getNumber() {
                return number;
                }
public synchronized void getStreet(int c) {
                while (!free) {
                        try {
                                wait();
                                } catch (InterruptedException e) {}
                        }
                free = false;
                carNumber = c;
}
public synchronized void releaseStreet(int c) {
                if (carNumber == c && !free) {
                        free = true;
                        notifyAll();
                        }
                }
}