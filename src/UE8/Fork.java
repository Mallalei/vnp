package UE8;

public class Fork {
    private int number;
    private int seat;
    private boolean free = true;

    public Fork(int n){
        number = n;
    }
    public synchronized void get(int s){
        while (!free){
            try {wait();}
            catch (InterruptedException e){}
        }
        free = false;
        seat = s;
        System.out.println("Philosopher " + seat +" gets fork " + number);
    }
    public synchronized void put(int s){
        if (seat == s && !free){
            free = true;
            System.out.println("Philosopher " + seat + " put fork "+ number);
            notifyAll();
        }
    }
}
