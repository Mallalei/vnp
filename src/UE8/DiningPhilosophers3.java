package UE8;

public class DiningPhilosophers3 {
    public static void main(String [] args) {
        Fork fork0 = new Fork(0);
        Fork fork1 = new Fork(1);
        Fork fork2 = new Fork(2);
        Fork fork3 = new Fork(3);
        new Philosopher3(0, fork3, fork0).start();
        new Philosopher3(1, fork0, fork1).start();
        new Philosopher3(2, fork1, fork2).start();
        new Philosopher3(3, fork2, fork3).start();
    }
}


