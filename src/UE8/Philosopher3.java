package UE8;

public class Philosopher3 extends Thread{
    private Fork leftFork, rightFork;
    private int counter;
private int seat;
// gibt an, ob der Philosoph zuerst die linke oder zuerst die rechte Gabel
        // nimmt (aendert sich bei jedem Durchlauf)
        private boolean leftForkFirst;
Philosopher3(int s, Fork r, Fork l) {
        seat = s;
        leftForkFirst = (seat % 2 == 0);
        leftFork = l;
        rightFork = r;
        }
@Override
public void run() {
        while (true) {
            think();
            if (leftForkFirst) {
                System.out.println("Philosopher "+seat+", LeftForkFirst: " + leftForkFirst);
                leftFork.get(seat);
                rightFork.get(seat);
                eat();
                rightFork.put(seat);
                leftFork.put(seat);
                } else {
                System.out.println("Philosopher "+seat+", LeftForkFirst: " + leftForkFirst);
                rightFork.get(seat);
                leftFork.get(seat);
                eat();
                leftFork.put(seat);
                rightFork.put(seat);
                }
            // Reihenfolge der Ressourcenanforderung wird im naechsten

            // Durchlauf umgekehrt
            leftForkFirst = !leftForkFirst;
            counter++;
            System.out.println("Zyklus: " + counter);
            }
        }
private void think() {
    System.out.println("Philosopher " + seat + " thinking");
        }
private void eat() {
        System.out.println("Philosopher " + seat + " eating");
        }
}
